from common.file_reader import RichEventReader

# Instantiate a RichEventReader pointing to /home/dcampora/rich_csv_subset/
# Apply displacement to be able to plot all pixels together
reader = RichEventReader(
  containing_folder="/home/dcampora/rich_csv_subset/",
  perform_pixel_displacement=True)

# Read one event
events = reader.read_events(range(0, 1))
event = events[0]

# Print general information about an event
print(event)
