#!/usr/bin/python3

import sys
sys.path.append("common/")

import os
import matplotlib.pyplot as plt
from event_model import *
from file_reader import RichEventReader

CONTAINING_FOLDER = "/home/dcampora/rich_csv_subset/"
OUTPUT_FOLDER = "figures/"
NUM_FILES_TO_PROCESS = 1
MIN_NUMBER_OF_HITS = 10
APPLY_DISPLACEMENT = True
RICH_LIMITS = {"bottom": -800, "top": 800, "left": -800, "right": 800}

def make_dir(dir):
  try:
    os.makedirs(dir)
  except:
    print("Warning:", dir, "exists. Files may be overwritten.")

def apply_limits(plt):
  limits = dict(RICH_LIMITS)
  plt.xlim(left=limits["left"], right=limits["right"])
  plt.ylim(bottom=limits["bottom"], top=limits["top"])

def plot_centroids(plt, track, scale, rich_number):
  # Plot centroid of segment
  for segment in track.segments():
    if segment.rich == rich_number:
      centroid_x, centroid_y = [], []
      if APPLY_DISPLACEMENT:
        centroid_x = [segment.plane_average_point[0]]
        centroid_y = [segment.plane_average_point[1]]
      else:
        centroid_x = [segment.plane_point_0[0], segment.plane_point_1[0]]
        centroid_y = [segment.plane_point_0[1], segment.plane_point_1[1]]
      plt.scatter(
        centroid_x,
        centroid_y,
        s=10*scale,
        color="red")

def plot_centroids_events(plt, event, scale, rich_number):
  reconstructed_tracks = [t for _, t in iter(event.tracks.items()) if \
    t.has_segment_in_rich(rich=rich_number) and len(t.pixels_from_panel(rich=rich_number)) > MIN_NUMBER_OF_HITS]
  for track in reconstructed_tracks:
    plot_centroids(plt, track, scale, rich_number)
  
reader = RichEventReader(
  containing_folder=CONTAINING_FOLDER,
  perform_pixel_displacement=APPLY_DISPLACEMENT)
events = reader.read_events(range(NUM_FILES_TO_PROCESS))
fig_scale = 1.5
scale = 1.5

for eventno in range(NUM_FILES_TO_PROCESS):
  event = events[eventno]

  # Make output folder
  output_top_folder = OUTPUT_FOLDER + str(eventno) + "/"
  make_dir(output_top_folder)

  # Print RICH scatterplots
  for rich_number in [1, 2]:
    rich_name = "rich" + str(rich_number)

    output_folder = output_top_folder + rich_name + "/"
    make_dir(output_folder)

    # All rich # pixels
    rich_pixels = event.pixels_from_panel(rich=rich_number)
    local_x = [a.local_corrected_position[0] for a in rich_pixels]
    local_y = [a.local_corrected_position[1] for a in rich_pixels]

    # Generate plot
    fig = plt.figure(figsize=(12*fig_scale, 9*fig_scale))
    ax = plt.axes()
    plt.scatter(
      local_x,
      local_y,
      s=10*scale
    )
    plot_centroids_events(plt, event, scale, rich_number)
    plt.xlabel('corrected local x', fontdict={'fontsize': 16*scale})
    plt.ylabel('corrected local y', fontdict={'fontsize': 16*scale})
    apply_limits(plt)
    outfile = output_folder + "scatter_" + rich_name + ".png"
    print("Generating", outfile)
    plt.savefig(outfile, bbox_inches='tight', pad_inches=0.2)
    plt.close()

    # Only reconstructed long tracks
    reconstructed_track_pixels = event.pixels_from_tracks(rich=rich_number)
    local_x = [a.local_corrected_position[0] for a in reconstructed_track_pixels]
    local_y = [a.local_corrected_position[1] for a in reconstructed_track_pixels]

    # Generate plot
    fig = plt.figure(figsize=(12*fig_scale, 9*fig_scale))
    ax = plt.axes()
    plt.scatter(
      local_x,
      local_y,
      s=10*scale
    )
    plot_centroids_events(plt, event, scale, rich_number)
    plt.xlabel('corrected local x', fontdict={'fontsize': 16*scale})
    plt.ylabel('corrected local y', fontdict={'fontsize': 16*scale})
    apply_limits(plt)
    outfile = output_folder + "scatter_reconstructible_" + rich_name + ".png"
    print("Generating", outfile)
    plt.savefig(outfile, bbox_inches='tight', pad_inches=0.2)
    plt.close()

    # Single track images
    reconstructed_tracks = [t for _, t in iter(event.tracks.items()) if t.has_segment_in_rich(rich=rich_number)]
    track_id = 0
    for track in reconstructed_tracks:
      track_pixels = track.pixels_from_panel(rich=rich_number)
      if len(track_pixels) > MIN_NUMBER_OF_HITS:
        local_x = [a.local_corrected_position[0] for a in track_pixels]
        local_y = [a.local_corrected_position[1] for a in track_pixels]

        # Generate plot
        fig = plt.figure(figsize=(12*fig_scale, 9*fig_scale))
        ax = plt.axes()
        plt.scatter(
          local_x,
          local_y,
          s=10*scale
        )
        plot_centroids(plt, track, scale, rich_number)
        plt.xlabel('corrected local x', fontdict={'fontsize': 16*scale})
        plt.ylabel('corrected local y', fontdict={'fontsize': 16*scale})
        apply_limits(plt)
        outfile = output_folder + rich_name + "_track_" + str(track_id) + ".png"
        track_id += 1
        print("Generating", outfile)
        plt.savefig(outfile, bbox_inches='tight', pad_inches=0.2)
        plt.close()
