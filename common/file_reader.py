import sys
sys.path.append("common/")

from event_model import Event
from collections import OrderedDict
import csv

def convert_to_number(s):
  if any([str(a) in s for a in range(0, 10)]):
    try:
      if "." in s: return float(s)
      else: return int(s)
    except:
      pass
  return s


class RichEventReader(object):
  def __init__(self,
    containing_folder="events/",
    debug=True,
    perform_pixel_displacement=True):

    # Settings for all readable files
    self.__containing_folder = containing_folder
    self.__filenames = {"MC particles": ("mc_particles", "MC particle index"),
      "MC pixel to particle": ("mc_pixel_to_particle", "pixel ID"),
      "MC track to particle": ("mc_track_to_particle", "track ID"),
      "Rich1 bottom": ("rich1_bottom", "pixel ID"),
      "Rich1 top": ("rich1_top", "pixel ID"),
      "Rich2 left": ("rich2_left", "pixel ID"),
      "Rich2 right": ("rich2_right", "pixel ID"),
      "Segments": ("segments", "segment ID")}
    self.__ordered_filenames = ["MC particles", "MC pixel to particle", "MC track to particle",
      "Rich1 bottom", "Rich1 top", "Rich2 left", "Rich2 right", "Segments"]

    # Container of all read events
    self.__event_dicts = []
    self.__debug = debug
    self.__perform_pixel_displacement = perform_pixel_displacement

  def __parse_events(self, events_to_read):
    for event_no in events_to_read:
      d = {}
      for key in self.__ordered_filenames:
        filename, key_id = self.__filenames[key]
        mc_file = self.__containing_folder + str(event_no) + "/" + filename + ".csv"

        if self.__debug:
          print("Reading", mc_file)
        
        reader = csv.DictReader(open(mc_file))
        
        d[filename] = OrderedDict()
        for row in reader:
          d[filename][convert_to_number(row[key_id])] = {k: convert_to_number(v) for k, v in iter(row.items())}
          
      self.__event_dicts.append(d)

  def read_events(self, event_number_list=[0]):
    previous_len = len(self.__event_dicts)
    self.__parse_events(event_number_list)
    return [Event(event_index, self.__event_dicts[previous_len + i], self.__filenames, perform_displacement=self.__perform_pixel_displacement)
      for event_index, i in zip(event_number_list, range(len(self.__event_dicts) - previous_len))]
