RICH python project
===================

This project consists of a small set of classes that allows to parse RICH CSV files.


Sample session
--------------

CSV data files (not included in this project) contains data from the RICH detectors and track reconstruction from events at the LHCb detector. A sample session is included with this project. You should first modify the location where the CSV files are, and then run it:

    python3 -i session.py

In the session, a single event is loaded. An event contains information about the pixels, tracks and their segments, and Monte Carlo associations between particles, tracks and pixels.

    >>> event.
    event.index                event.pixel_count          event.pixels_from_panel(   event.segments             
    event.mc_particles         event.pixels               event.pixels_from_tracks(  event.tracks               

The CSVs contain information about the two RICH subdetectors of LHCb. Each RICH subdetector is divided in two parts. That is why the pixel count reports four sizes (RICH1 bottom, RICH1 top, RICH2 left, RICH2 right):

    >>> event.pixel_count
    [1611, 1865, 1161, 1328]

Each pixel contains information about its coordinates in the global coordinate system, in the local coordinate system, and in the local coordinate system after applying a correction. In addition, pixels contain raw data about their position in detector specific datatypes (PDCol, pixelRow, PDNumInCol, pixelCol). It is possible to retrieve the MC particles that generated this pixel, as well as its detector (RICH1, RICH2) and side of the detector.

    >>> p = event.pixels[0]
    >>> p.
    p.PDCol                     p.global_position           p.local_corrected_position  p.mc_particles(             p.pixelRow                  p.side
    p.PDNumInCol                p.index                     p.local_position            p.pixelCol                  p.rich

Tracks are particle trajectories that were reconstructed by an LHCb tracking algorithm. Only the relevant information of tracks is included in this event model. A track contains information about its segments (it may have one in RICH1 and one in RICH2). Since the Monte Carlo simulated data is included, it is also possible to know what its associated pixels should be, and what Monte Carlo particles are associated to it.

    >>> t = event.tracks[5]
    >>> t.
    t.has_segment_in_rich(  t.index                 t.mc_particles(         t.pixels(               t.pixels_from_panel(    t.segments(             t.track_type
    >>> t.pixels()
    [Pixel #1652 rich1 bottom (115.97, 14.18), Pixel #1653 rich1 bottom (124.48, 5.37), Pixel #1692 rich1 bottom (18.69, 17.12), Pixel #1750 rich1 bottom (34.86, 25.03), Pixel #1751 rich1 bottom (40.54, 25.03), Pixel #1768 rich1 bottom (89.91, 25.03), Pixel #1830 rich1 bottom (-4.54, -3.44), Pixel #2040 rich1 bottom (135.84, -109.96), Pixel #2041 rich1 bottom (130.16, -112.90), Pixel #2042 rich1 bottom (130.16, -115.83), Pixel #2071 rich1 bottom (149.17, -84.42), Pixel #2100 rich1 bottom (152.01, -44.20), Pixel #2101 rich1 bottom (152.01, -47.13), Pixel #2135 rich1 bottom (146.33, -30.40), Pixel #2345 rich1 bottom (-13.05, -98.21), Pixel #2347 rich1 bottom (-15.89, -98.21), Pixel #2403 rich1 bottom (-21.57, -75.61), Pixel #2406 rich1 bottom (-21.57, -69.74), Pixel #2407 rich1 bottom (-24.40, -75.61), Pixel #2451 rich1 bottom (-21.57, -55.94), Pixel #2452 rich1 bottom (-21.57, -44.20), Pixel #2485 rich1 bottom (-13.05, -15.72), Pixel #2486 rich1 bottom (-10.21, -12.78), Pixel #2487 rich1 bottom (-15.89, -24.53), Pixel #2776 rich1 bottom (115.97, -131.05), Pixel #2782 rich1 bottom (118.81, -128.11), Pixel #2871 rich1 bottom (51.89, -142.79), Pixel #2877 rich1 bottom (34.86, -142.79), Pixel #2878 rich1 bottom (37.70, -139.86), Pixel #2891 rich1 bottom (79.41, -142.79), Pixel #2911 rich1 bottom (104.10, -136.92)]

A track segment contains information about its momentum and location.

    >>> s = t.segments()[0]
    >>> s.
    s.average_photon_energy  s.index                  s.middle_point           s.plane_average_point    s.plane_point_1          s.rich                   
    s.csv_index              s.middle_momentum        s.momentum(              s.plane_point_0          s.radiator               s.track(                 

Finally, a Monte Carlo particle is tagged with a particle ID, and knows about its associated pixels and tracks:

    >>> p = event.mc_particles[0]
    >>> p.
    p.PID      p.index    p.pixels(  p.tracks(  


Generating images
-----------------

It is possible to visualize some events by running the visualize script:

    python3 common/visualize.py


Expected output
---------------

Once that a reconstruction algorithm is run, an output following the format below is expected:

    Predictions (%tot) | electron    kaon      muon      pion     proton  | Purity (%)
    -------------------+--------------------------------------------------+-----------
    electron           |   0.034     0.034     0.000     2.412     0.067  |   1.316 
    kaon               |   0.000    10.084     0.000     5.829     2.178  |  55.741 
    muon               |   0.000     0.000     0.000     0.369     0.034  |   0.000 
    pion               |   0.034     1.374     0.000    66.533     1.005  |  96.501 
    proton             |   0.000     2.647     0.000     4.322     3.049  |  30.435 
    -------------------+--------------------------------------------------+-----------
    Efficiency (%)     |  50.000    71.327     0.000    83.727    48.148  |
    -------------------+--------------------------------------------------+-----------
    ID eff (%)         |  K->K,Pr,D :  90.047  pi->e,m,pi :  87.226
    MisID eff (%)      |  K->e,m,pi :   9.953  pi->K,Pr,D :  12.774                               
